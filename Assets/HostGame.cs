﻿using Mirror;
using UnityEngine;

public class HostGame : MonoBehaviour{
    [SerializeField] private uint roomSize = 10;
    private string roomName;
    private NetworkManager networkManager;
    private void Start(){
        networkManager = NetworkManager.singleton;
        // if(networkManager.matchMaker == null)
        //     networkManager.StartMatchMaker();
    }
    public void SetRoomName(string _name){
        roomName = _name;
    }
    public void SetRoomSize(uint _size){
        roomSize = _size;
    }
    public void CreateRoom(){
        if(roomName != "" && roomName != null){
            Debug.Log("A party has been created : " + roomName + " with " + roomSize + " slots");
            // Create party

        }
    }



}
