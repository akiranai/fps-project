﻿using UnityEngine;
using Mirror;
using System.Collections;
[RequireComponent(typeof(PlayerSetup))]
public class Player : NetworkBehaviour {

    [SerializeField] private int maxHealth = 100;
    [SyncVar] private int currentHealth;
    [SyncVar] private bool _isDead = false;
        /*
        Get a value and change this value if the call got the autorisation
    */
    public bool isDead{
        get { return _isDead;}
        protected set {_isDead = value;}
    }
    [SerializeField] private Behaviour[] disableOnDeath;
    [SerializeField] private GameObject[] disableGameObjectOnDeath;
    [SerializeField] private Animator playerAnimationController;
    [SerializeField] private GameObject respawnEffect;
    [SerializeField] private GameObject dieEffect;
    private bool[] wasEnabled;
    private bool firstSetup = true;

    /// <summary>
    ///  Define which componenents should be de-activated
    /// </summary>
    public void SetupPlayer(){
        if(isLocalPlayer){
            GameManager.instance.SetSceneCameraActive(false);
            GetComponent<PlayerSetup>().playerUIInstance.SetActive(true);
            }
        CmdBroadCastNewPlayerSetup();
    }
    [Command]
    private void CmdBroadCastNewPlayerSetup(){
        RpcSetupPlayerOnAllClients();
    }
    [ClientRpc]
    private void RpcSetupPlayerOnAllClients(){
        if(firstSetup){
            wasEnabled = new bool[disableOnDeath.Length];
            for (int i = 0; i < disableOnDeath.Length; i++)
                wasEnabled[i] = disableOnDeath[i].enabled;  //If activated true otherwise false
            SetDefaults();
            firstSetup = false;
            }
    }
    // test function
    private void Update(){
         if(!isLocalPlayer)
             return;
         if(Input.GetKeyDown(KeyCode.K))
             RpcTakeDamage(9999);
     }
    // ClientRPC -> Called on every instance
    /*
        When taking damage, reduce health ...
    */
    [ClientRpc]
    public void RpcTakeDamage(int _amount){
        if(isDead)
            return;
        currentHealth -= _amount;
        Debug.Log(transform.name + "a maintenant : " + currentHealth + " points de vie.");
        
        if(currentHealth <= 0 )
            Die();
    }
    /*
        DIE FUNCTION - If player die ... kekw
    */
    private void Die(){
        isDead = true;
        // Player particle on death
        GameObject _gfxIns = (GameObject)Instantiate(dieEffect, transform.position, transform.rotation);
        Destroy(_gfxIns, 2f);

        // Switch camera 
        if(isLocalPlayer){
            GameManager.instance.SetSceneCameraActive(true);
            GetComponent<PlayerSetup>().playerUIInstance.SetActive(false);
    }

        playerAnimationController.SetBool("isDying", true); // Set dead animation
        //  DISABLE PLAYER'S COMPONENTS (fire-move-guns-...)
        for (int i = 0; i < disableOnDeath.Length; i++)
            disableOnDeath[i].enabled = false;
        // DISABLE PLAYER'S GAMEOBJECT wait a bit
        StartCoroutine(disableGameObjectOnDie());

        Collider _col = GetComponent<CapsuleCollider>();
        if (_col != null)
            _col.enabled = false;
        Debug.Log(transform.name + " is dead");
        
;        // CALL RESPAWN FUNCTION
        StartCoroutine(Respawn());

    }
    IEnumerator disableGameObjectOnDie(){
        yield return new WaitForSeconds(1.5f);
        for (int i = 0; i < disableGameObjectOnDeath.Length; i++)
            disableGameObjectOnDeath[i].SetActive(false);

    }
    IEnumerator Respawn(){
        yield return new WaitForSeconds(GameManager.instance.matchSettings.respawnTime);
        playerAnimationController.SetBool("isDying", false);
        Transform _startPoint = NetworkManager.singleton.GetStartPosition();
        yield return new WaitForSeconds(0.1f);
        GameObject _gfxIns = (GameObject)Instantiate(respawnEffect, _startPoint.position, _startPoint.rotation);
        Destroy(_gfxIns, 3f);
        transform.position =  _startPoint.position;
        transform.rotation =  _startPoint.rotation;
        yield return new WaitForSeconds(3f);
        SetupPlayer();
        Debug.Log(transform.name + " respawned");
        
    }
    /*
        Default parameters
    */
    public void SetDefaults(){
        isDead = false;
        currentHealth = maxHealth;
        // Reaneble component that were disable
        for (int i = 0; i < disableOnDeath.Length; i++)
            disableOnDeath[i].enabled = wasEnabled[i];
        // Reanable gameobject that were disable
        for (int i = 0; i < disableGameObjectOnDeath.Length; i++)
            disableGameObjectOnDeath[i].SetActive(true);
        // can't store collider in behaviour
        // to check right collider 
        Collider _col = GetComponent<CapsuleCollider>();
        if (_col != null)
            _col.enabled = true;
        // Switch camera

    }

}