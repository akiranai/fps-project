﻿using UnityEngine;
using Mirror;
// Is local player 
[RequireComponent(typeof(Player))]

public class PlayerSetup : NetworkBehaviour{
    [SerializeField] Behaviour[] componentsToDisable; // Componenent to disable ... kekw
    [SerializeField] private string remoteLayerName = "RemotePlayer";
    [SerializeField] private string dontDrawLayerName ="DontDraw";
    [SerializeField] private GameObject playerGraphics;
    [SerializeField] private GameObject playerUIPrefab;
    [HideInInspector] public GameObject playerUIInstance;
    
    // Camera sceneCamera;
    private void Start(){
        // if not current player
        if(!isLocalPlayer){
            DisableComponents();
            AssignRemoteLayer();
        }
        else{
            // sceneCamera = Camera.main;
            // if(sceneCamera != null)
            //     sceneCamera.gameObject.SetActive(false);
            
            // Disable graphical local player parts
            SetLayerRecursively(playerGraphics, LayerMask.NameToLayer(dontDrawLayerName));
            // SetLayerRecursively(playerGraphicsExt, LayerMask.NameToLayer(dontDrawLayerNameExt));
            
            //create user UI
            playerUIInstance = Instantiate(playerUIPrefab);
            playerUIInstance.name = playerUIPrefab.name;


        }
        GetComponent<Player>().SetupPlayer();
    }
    /*
        Disable recusively graphics elements by changing layer of each child
    */
    private void SetLayerRecursively(GameObject obj, int newLayer){
        obj.layer = newLayer;
        foreach (Transform child in obj.transform)
            SetLayerRecursively(child.gameObject, newLayer);
           
    }
    /*Fct built in
    Getting read when client's connecting    
    */
    public override void OnStartClient(){  
        
        base.OnStartClient();
        string _netID = GetComponent<NetworkIdentity>().netId.ToString();
        Player _player = GetComponent<Player>();
        GameManager.RegisterPlayer(_netID, _player);
    }
    /*
    Attribute player layer who's not local one
    */
    private void AssignRemoteLayer(){
        gameObject.layer = LayerMask.NameToLayer(remoteLayerName);
        //foreach (GameObject x in gameObjectToChangeLayer)
        //    x.layer = LayerMask.NameToLayer(dontDrawLayerName);
        
    }
    private void DisableComponents(){
        // Loop for disabling componements for others players
        for(int i = 0; i < componentsToDisable.Length; i++)
            componentsToDisable[i].enabled = false;
    }
    private void OnDisable(){
        Destroy(playerUIInstance);
        /*  OLD SCENE CAMERA */
        // if(sceneCamera != null)
        //     sceneCamera.gameObject.SetActive(true);
        if (isLocalPlayer)
            GameManager.instance.SetSceneCameraActive(true);
        GameManager.UnrRegisterPlayer(transform.name); //lookup in dict following the name
    }
}
