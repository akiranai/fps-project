﻿using UnityEngine;
[System.Serializable]
public class PlayerWeapon{
    public string name  = "Glock";      // Weapon's name
    public int damage    = 10;          // Weapon's damage
    public float range  = 100f;         // Weapon's range projectil
    public float fireRate = 0f;
    public GameObject graphics;

}
