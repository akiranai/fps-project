﻿using UnityEngine;
using Mirror;
//[RequireComponent(typeof(WeaponManager))]
public class PlayerShoot : NetworkBehaviour{
    // Start is called before the first frame update
    [SerializeField] private Camera cam; // player's camera
    [SerializeField] private LayerMask mask;
    private WeaponManager weaponManager;
    private PlayerWeapon currentWeapon;
    void Start(){
        if(cam == null){
                Debug.LogError("No referenced camera");
                this.enabled = false; // disable this script
            }
        
        weaponManager = GetComponent<WeaponManager>();
    }
    /// <summary>
    /// Update is called once per frame
    /// </summary>
    private void Update(){
        currentWeapon = weaponManager.GetCurrentWeapon();
        if(currentWeapon.fireRate <= 0f){
            if(Input.GetButtonDown("Fire1"))
                Shoot();
        }
        else{
            if(Input.GetButtonDown("Fire1"))
                InvokeRepeating("Shoot", 0f, 1f/currentWeapon.fireRate);
            else if(Input.GetButtonUp("Fire1"))
                CancelInvoke("Shoot");
        }
        
    }
    /* Fct called on server when player shoot */
    [Command]
    void CmdOnShoot(){
        RpcDoShootEffects();
    }
    /* Fct called when player hit something */
    [Command]
    void CmdOnHit(Vector3 _pos, Vector3 _normal){
        RpcDoHitEffect(_pos, _normal);
    }

    [ClientRpc]
    void RpcDoHitEffect(Vector3 _pos, Vector3 _normal){
        // Optimisation purpose ->Put it in gameobject then destroy 
        GameObject _hitEffect = (GameObject)Instantiate(weaponManager.GetCurrentGraphics().hitEffectPrefab, _pos, Quaternion.LookRotation(_normal));
        Destroy(_hitEffect, 2f);
    }  
    /* show up partical effect (fire) in each clients */
    [ClientRpc]
    void RpcDoShootEffects(){
        weaponManager.GetCurrentGraphics().muzzleFlash.Play();
        Debug.Log(" MUZZLEFLASH EFFECT");
    }
    //ONLY CLIENT SIDE
    [Client]
    private void Shoot(){
        if(!isLocalPlayer)
            return;
        // call when local player shoot
        CmdOnShoot();
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, currentWeapon.range, mask)){
            //Only read if hit smthing
            if(hit.collider.tag == "Player")
                CmDPlayerShot(hit.collider.name, currentWeapon.damage);
            if(hit.rigidbody)
                 hit.rigidbody.AddForce(ray.direction * 200f);
            CmdOnHit(hit.point, hit.normal);
        }
    }
    //SENT INFO TO SERVERR
    //WHO HIT WHO
    [Command]
    private void CmDPlayerShot(string _playerID, int _damage){
        Debug.Log(_playerID + " got hit");
        Player _player = GameManager.GetPlayer(_playerID);
        _player.RpcTakeDamage(_damage);
    }
}
