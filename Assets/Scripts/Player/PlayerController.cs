﻿using System.Collections;
using Mirror;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerController: NetworkBehaviour{

    
    public CharacterController controller;
    [SerializeField] private float playerSpeed = 10f;
    [SerializeField] private float gravity = -9.81f;
    [SerializeField] private float jumpHeight = 7f;
    //
    public Transform groundCheck;
    [SerializeField] private float groundDistance = 0.4f;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private GameObject dashEffect;
    [SerializeField] private Transform dashEffectPosition;
    private bool canJump = true;
    private bool canDash = true;
    
    Vector3 velocity;
    bool isGrounded;

    // Sprint -- WONT USE SPRINT ANYMORE
    // [SerializeField]
    // private float sprint = 12f;
    // private int staminaSprint = 10; // SprintDuration in second
    // private int staminaSprintStartValue = 10; 
    // private bool isSprinting = true;
    // private bool canSprint = true;
    public Animator anim;

    private void Start(){
        anim = GetComponent<Animator>();
        dashEffect.SetActive(false);
    }
    void Update(){
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        
        if(isGrounded && velocity.y < 0){
            velocity.y = -2f;
            velocity.x = 0f;
            velocity.z = 0f;   
        }
        //if(isGrounded && )
        //    anim.SetTrigger("landing"); 

        float horizontalMove    = Input.GetAxis("Horizontal");
        float verticalMove      = Input.GetAxis("Vertical");
        // Simple movement
        Vector3 move = transform.right * horizontalMove + transform.forward * verticalMove;
        anim.SetFloat("velX", horizontalMove);
        anim.SetFloat("velY", verticalMove);
        anim.SetFloat("isWalkingWithWeapon", verticalMove);
        // Jump 
        if(Input.GetButtonDown("Jump") && isGrounded && canJump){
            anim.SetTrigger("Jumping");
            velocity.y += Mathf.Sqrt(jumpHeight * -2f * gravity);
            StartCoroutine(jumpCoroutine());
            }
            
        // Dash
        if(Input.GetButtonDown("Dash") && canDash){
            StartCoroutine(dashCoroutine(move));
        }
        controller.Move(move * playerSpeed * Time.deltaTime);
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }
    IEnumerator jumpCoroutine(){
        //Print the time of when the function is first called.
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        //yield on a new YieldInstruction that waits for x seconds.
        canJump = false;
        yield return new WaitForSeconds(0.6f);
        anim.SetTrigger("Landing"); 
        canJump = true;
        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }
    IEnumerator dashCoroutine(Vector3 move){
        dashEffect.SetActive(true);
        float old_speed = playerSpeed;
        playerSpeed = 50f;
        canDash = false;
        yield return new WaitForSeconds(0.2f);
        playerSpeed = old_speed;
        yield return new WaitForSeconds(0.25f);
        dashEffect.SetActive(false);
        yield return new WaitForSeconds(1.8f);
        canDash = true;
    }
    public void jumpBlock(float jumpBlockHeight){
        velocity.y += Mathf.Sqrt(jumpBlockHeight * -2f * gravity);
        StartCoroutine(jumpCoroutine());
    }
    public void wallJumpBlock(float bump){
        if(Input.GetButtonDown("Jump")){
            anim.SetTrigger("Jumping");
            velocity.y += Mathf.Sqrt(bump * -2f * gravity);
            }
    }
    private void OnControllerColliderHit(ControllerColliderHit obj){
        //Debug.Log(obj.gameObject.name);
        if(obj.gameObject.tag == "jumpblock"){
            Debug.Log("JUMPBLOCK");
            jumpBlock(10f);
        }
        if(obj.gameObject.tag == "wallJump"){
             Debug.Log("wallJUMP");
             wallJumpBlock(2f);
        }
    }

}