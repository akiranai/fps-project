﻿using UnityEngine;
/**
* Follow the mouse direction following the look of the player
*
*/
public class mouseLook : MonoBehaviour
{
    [SerializeField] private float mouseSensitivity = 100f;
    // Start is called before the first frame update
    [SerializeField] private Transform playerBody;
    [SerializeField] private Transform spin;
    [SerializeField] private float minAngle = -90f;
    [SerializeField] private float maxAngle = 90f;
    float xRotation = 0f;
    float zRotation = 0f;
    void Start(){
        // hide and lock our cursor to the center of the screen
        Cursor.lockState = CursorLockMode.Locked;
    }
    void Update(){
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, minAngle, maxAngle);
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f); // x y z
        playerBody.Rotate(Vector3.up * mouseX);
    }
    // Spin transform
    void LateUpdate(){
        zRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        zRotation = Mathf.Clamp (zRotation, minAngle, maxAngle);
        spin.localEulerAngles = new Vector3(zRotation, spin.localEulerAngles.x, 0f);
    }
}
