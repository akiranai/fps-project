﻿using UnityEngine;
using System.Collections.Generic;
public class GameManager : MonoBehaviour{
    #region 
    public static GameManager instance; 
    public MatchSettings matchSettings;
    [SerializeField] private GameObject sceneCamera;
    private void Awake(){
        if(instance !=null)
            Debug.LogError("More than 1 object of GameManager in the scene");
        else
            instance = this;
    }
    #endregion
    #region player tracking
    //Attribute prefix to all player
    private const string PLAYER_ID_PREFIX = "Player ";
    private static Dictionary<string, Player> players= new Dictionary<string, Player>();
    public static void RegisterPlayer(string _netID, Player _player){
        string _playerID = PLAYER_ID_PREFIX + _netID;
        players.Add(_playerID, _player);
        _player.transform.name = _playerID;
    }
    public static void UnrRegisterPlayer(string _playerID){
        players.Remove(_playerID);
    }

    public static Player GetPlayer(string _playerID){
        return players[_playerID];
    }
    public void SetSceneCameraActive(bool isActive){
        if(sceneCamera == null)
            return;
        sceneCamera.SetActive(isActive);
    }
    #endregion
//     private void OnGui(){
//         GUILayout.BeginArea(new Rect(0,0,200,500));
//         GUILayout.BeginVertical();
//         foreach(string _playerID in players.Keys){
//             GUILayout.Label(_playerID +  "  -  " + players[_playerID].transform.name);
//         }

//         GUILayout.EndVertical();
//         GUILayout.EndArea();
//     }
 }
