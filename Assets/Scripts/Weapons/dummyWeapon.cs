using UnityEngine;

public class dummyWeapon : MonoBehaviour{
    // Start is called before the first frame update
    [SerializeField] private GameObject owner;
    [SerializeField] private Transform attachPoint;
    void Start(){
        //if (owner != null)
        //    transform.localScale = owner.transform.localScale;

    }
    private void Update(){
        if(owner != null){
            transform.parent = attachPoint.transform;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(-51.752f, -44.96f, 25.734f);
            transform.localScale    = new Vector3(3,3,3);
        }
        else
            Destroy(gameObject);
    }
}