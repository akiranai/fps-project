﻿using UnityEngine;
using Mirror;
public class WeaponManager : NetworkBehaviour{

    [SerializeField] private PlayerWeapon primaryWeapon;
    [SerializeField] private string weaponLayerName = "Weapon";
    [SerializeField] private Transform weaponHolder;
    private PlayerWeapon currentWeapon;
    private WeaponGraphics currentGraphics;
    void Start(){
        EquipeWeapon(primaryWeapon);
    }
    /* Return current weapon*/
    public PlayerWeapon GetCurrentWeapon(){
        return currentWeapon;
    }

    public WeaponGraphics GetCurrentGraphics(){
        return currentGraphics;
    }
    void EquipeWeapon(PlayerWeapon _weapon){
        currentWeapon = _weapon;
        GameObject _weaponIns = (GameObject)Instantiate(_weapon.graphics, weaponHolder.position, weaponHolder.rotation);
        _weaponIns.transform.SetParent(weaponHolder);

        currentGraphics = _weaponIns.GetComponent<WeaponGraphics>();
        if(currentGraphics == null)
            Debug.LogError("Pas de script WeaponGraphics sur l'arme: " + _weaponIns.name);

        if (isLocalPlayer)
            Util.SetLayerRecursively(_weaponIns, LayerMask.NameToLayer(weaponLayerName));
    }

}
