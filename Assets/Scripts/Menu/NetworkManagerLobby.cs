﻿using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
public class NetworkManagerLobby : NetworkManager
{
    [SerializeField] private int minPlayers = 2;
    // reference by string
    [Scene] [SerializeField] private string menuScene = string.Empty;
   
    [Header("Maps")]
    [SerializeField] private int numberOfRounds = 1;
    [SerializeField] private MapSet mapSet = null;

    [Header("Room")]
    [SerializeField] private NetworkRoomPlayerLobby roomPlayerPrefab = null;

    [Header("Game")]
    [SerializeField] private NetworkGamePlayerLobby gamePlayerPrefab = null;
    [SerializeField] private GameObject playerSpawnSystem = null;
    [SerializeField] private GameObject roundSystem = null;

    private MapHandler mapHandler;

    public static event Action OnClientConnected;
    public static event Action OnClientDisconnected;
    public static event Action<NetworkConnection> OnServerReadied;
    public static event Action OnServerStopped;

    public List<NetworkRoomPlayerLobby> RoomPlayers { get; } = new List<NetworkRoomPlayerLobby>();
    public List<NetworkGamePlayerLobby> GamePlayers { get; } = new List<NetworkGamePlayerLobby>();

    public override void OnStartServer() => spawnPrefabs = Resources.LoadAll<GameObject>("SpawnablePrefabs").ToList();

    public override void OnStartClient(){
        var spawnablePrefabs = Resources.LoadAll<GameObject>("SpawnablePrefabs");

        foreach (var prefab in spawnablePrefabs)
            ClientScene.RegisterPrefab(prefab);
    }
    /// <summary>
    /// When the client's connecting
    /// </summary>
    /// <param name="conn"></param>
    public override void OnClientConnect(NetworkConnection conn){
        base.OnClientConnect(conn);
        // if something wrong happend
        OnClientConnected?.Invoke();
    }
    /// <summary>
    /// When the client's desconnecting
    /// </summary>
    /// <param name="conn"></param>
    public override void OnClientDisconnect(NetworkConnection conn){
        base.OnClientDisconnect(conn);
        OnClientDisconnected?.Invoke();
    }
    /// <summary>
    /// Server side, on connection, check if not too much player nor the game is in progress
    /// </summary>
    /// <param name="conn"></param>
    public override void OnServerConnect(NetworkConnection conn){
        // If to many player, disconnect this one
        if (numPlayers >= maxConnections){
            conn.Disconnect();
            return;
        }
        // If the game is in progress 
        // #todo make it depending the mod
        if (SceneManager.GetActiveScene().name != menuScene){
            conn.Disconnect();
            return;
        }
    }
    /// <summary>
    /// add a player on server side
    /// </summary>
    /// <param name="conn"></param>
    public override void OnServerAddPlayer(NetworkConnection conn){
        if (SceneManager.GetActiveScene().name == menuScene){
            bool isLeader = RoomPlayers.Count == 0;
            NetworkRoomPlayerLobby roomPlayerInstance = Instantiate(roomPlayerPrefab);
            roomPlayerInstance.IsLeader = isLeader;
            NetworkServer.AddPlayerForConnection(conn, roomPlayerInstance.gameObject);
        }
    }
    /// <summary>
    /// If the server it self go out
    /// </summary>
    /// <param name="conn"></param>
    public override void OnServerDisconnect(NetworkConnection conn){
        if (conn.identity != null){
            var player = conn.identity.GetComponent<NetworkRoomPlayerLobby>();
            RoomPlayers.Remove(player);
            NotifyPlayersOfReadyState();
        }
        base.OnServerDisconnect(conn);
    }

    public override void OnStopServer(){
        OnServerStopped?.Invoke();
        RoomPlayers.Clear();
        GamePlayers.Clear();
    }

    public void NotifyPlayersOfReadyState(){
        foreach (var player in RoomPlayers)
            player.HandleReadyToStart(IsReadyToStart());
    }

    private bool IsReadyToStart()
    {
        if (numPlayers < minPlayers) 
            return false;

        foreach (var player in RoomPlayers)
            if (!player.IsReady) { return false; }

        return true;
    }

    public void StartGame(){
        if (SceneManager.GetActiveScene().name == menuScene){
            if (!IsReadyToStart()) 
                return;

            mapHandler = new MapHandler(mapSet, numberOfRounds);

            ServerChangeScene(mapHandler.NextMap);
        }
    }

    public override void ServerChangeScene(string newSceneName)
    {
        // From menu to game
        if (SceneManager.GetActiveScene().name == menuScene && newSceneName.StartsWith("Scene_Map")){
            for (int i = RoomPlayers.Count - 1; i >= 0; i--){
                var conn = RoomPlayers[i].connectionToClient;
                var gameplayerInstance = Instantiate(gamePlayerPrefab);
                gameplayerInstance.SetDisplayName(RoomPlayers[i].DisplayName);

                NetworkServer.Destroy(conn.identity.gameObject);

                NetworkServer.ReplacePlayerForConnection(conn, gameplayerInstance.gameObject);
            }
        }

        base.ServerChangeScene(newSceneName);
    }

    public override void OnServerSceneChanged(string sceneName){
        if (sceneName.StartsWith("Scene_Map")){
            GameObject playerSpawnSystemInstance = Instantiate(playerSpawnSystem);
            NetworkServer.Spawn(playerSpawnSystemInstance);

            GameObject roundSystemInstance = Instantiate(roundSystem);
            NetworkServer.Spawn(roundSystemInstance);
        }
    }

    public override void OnServerReady(NetworkConnection conn){
        base.OnServerReady(conn);
        OnServerReadied?.Invoke(conn);
    }
}