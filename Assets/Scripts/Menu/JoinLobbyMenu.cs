﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class JoinLobbyMenu : MonoBehaviour{
    // reference to network manager...
    [SerializeField] private NetworkManagerLobby networkManager = null;

    [Header("UI")]
    [SerializeField] private GameObject landingPagePanel = null;
    // Input field where we put the IP address
    [SerializeField] private TMP_InputField ipAddressInputField = null;
    // Just the join button
    [SerializeField] private Button joinButton = null;
    // Sub-unsub scribe to the event
    private void OnEnable(){
        NetworkManagerLobby.OnClientConnected += HandleClientConnected;
        NetworkManagerLobby.OnClientDisconnected += HandleClientDisconnected;
    }

    private void OnDisable(){
        NetworkManagerLobby.OnClientConnected -= HandleClientConnected;
        NetworkManagerLobby.OnClientDisconnected -= HandleClientDisconnected;
    }
    /// <summary>
    /// When join lobby button is triggerd, check the IP address in the INPUT field and try to start the client
    /// </summary>
    public void JoinLobby(){
        string ipAddress = ipAddressInputField.text;

        networkManager.networkAddress = ipAddress;
        networkManager.StartClient();
        // Avoid spam click
        joinButton.interactable = false;
    }
    /// <summary>
    /// If succefully connect 
    /// </summary>
    private void HandleClientConnected(){
        // Just re-enable the button "join"
        joinButton.interactable = true;
        // Disable those object to get the stage when in the main menu
        gameObject.SetActive(false);
        landingPagePanel.SetActive(false);
    }
    /// <summary>
    /// Turn on join button when disconnect
    /// </summary>
    private void HandleClientDisconnected(){
        joinButton.interactable = true;
    }
}