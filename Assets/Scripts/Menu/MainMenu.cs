﻿  
using UnityEngine;

public class MainMenu : MonoBehaviour{
    // Reference to networmanager
    [SerializeField] private NetworkManagerLobby networkManager = null;
    [Header("UI")]
    [SerializeField] private GameObject landingPagePanel = null;
    /// <summary>
    /// When click host button.
    /// </summary>
    public void HostLobby(){
        networkManager.StartHost();
        landingPagePanel.SetActive(false);
    }
}
